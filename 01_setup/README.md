
---
# Introduction

---

# Pre requirements

- Install minikube or use your own k8s cluster
- Configure access to minikube or to your k8s cluster
- Setup `Helm` to work with minikube or your k8s cluster

> `[!]` Note: if you have k8s cluster on any platform, no need to install docker and minikube

---

# Install minikube

Minikube is an open-source tool that simulates single-node Kubernetes cluster on your local machine. In these next pages, we will see how to install minikube on most of Linux distributions and configure Helm to work with it. 
In order to install minikube we need one of the emulation platforms to lean on. Possible platforms are:

- Docker : the easiest one because it can work inside VM as well
- Virtualbox: the good old one if VM's scare you
- KVM: if you are native Linux user
- HyperV: for those of you who wouldn't leave windows

As mentioned, we'll be using `docker` and here is how to install it in the most easiest way known to us:

```sh
curl -L get.docker.com |  sudo bash 
```

> `[!]` Note: For windows, please google the setup instructions

---
# Install minikube (cont.)

Let's get the minikube executable, give it permissions and let's start using it.

```sh
which wget 
sudo wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 -O /usr/local/bin/minikube
sudo chmod +x  /usr/local/bin/minikube
sudo chown ${USER}:${USER} /usr/local/bin/minikube
```
Once the you have downloaded the minikube executable, we can enable it to run on the emulation platform by starting it
```sh
minikube version
minikube start --vm-driver=docker
```
> `[!]` Note: usually minikube can detect the emulation environment by itself, but if you have several of them installed it might not work thus please us `--vm-driver=YOUR-EMULATION-ENV`
> 
---
# Install minikube (cont.)

Some minimalistic CLI options for Minikube:
- `minikube start` : Start Minikube on platform running platform, in our case docker
- `minikube stop` : Stop minikube
- `minikube status` : Show status
- `minikube delete` : Remove minikube cluster
- `minikube ip` : Show the IP address of the specified node
- `minikube ssh` : Possible to access with SSH to the VM
- `minikube service ` :  Returns a URL to connect to a service
- `minikube addons list` : List a minikube addon


---
# Install `Helm`

```sh
curl -O https://raw.githubusercontent.com/Helm/Helm/master/scripts/get-Helm-3 
bash ./get-Helm-3 
Helm version 
```

---

# What is `Helm` ?

By a definition, `Helm` is the package management system for Kubernetes. Just like `npm` for `nodejs`, `pip3` for `python3` or even `apt` for Debian Linux, `Helm` enables us to deploy applications as if they were packages to our kubernetes cluster.

Without `Helm`, deploying an application to a Kubernetes cluster means copying and pasting lots of yaml manifest files and manually running `kubectl apply -f ` over and over again to create the right Kubernetes deployments services and pods.Helm packages all these things together in one chart and allows you to install with just one command.

`Helm` also gives you the option to create chart versions, upgrade versions, debug deployments, and roll back as needed. `Helm`'s templating engine lets you pass in data and render values dynamically, meaning you can deploy the same application in many environments using different sets of values. Lots of useful third party software like **Datadog**, **Redis** and **Nginx** have `Helm` charts, which you can customize and deploy. `Helm` will make deploying your workloads faster and safer. 

---


# What `Helm` can do ?

Helm allows software developers to deploy and test an environment in the simplest possible way. Less time is needed to get from development to testing to production.

Besides boosting productivity, Helm presents a convenient way for developers to pack and send applications to end users for installation.

---

# How does `Helm` work ?

<img src="../99_misc/.img/helm_arch.png" alt="Helm_arch" style="float:right;width:400px;">

Helm and Kubernetes work like a client/server application. The Helm client pushes resources to the Kubernetes cluster. The server-side depends on the  version: Helm 2 uses Tiller while Helm 3 got rid of Tiller and entirely relies on the Kubernetes API

