# Helm Shallow Dive

.footer: Created By Alex M. Schapelle, VAIOLabs.io

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is Helm ?
- Who needs Helm ?
- How Helm works ?
- How to manage Helm in various scenarios ?

---

### Who Is This course for ?

- Junior/senior devops who have no knowledge of k8s deployment
- for junior/senior developers who are still developing on different stacks to containers
- Experienced ops who need refresher k8s deployment with Helm


---

# Course Topics

- Intro
- Helm internals
- Helm basics:
    - Setup Helm
    - Configuration of repository
    - Helm charts
    - Deployments
- Helm advance features
