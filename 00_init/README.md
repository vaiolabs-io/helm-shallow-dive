# Helm Shallow Dive



.footer: Created By Alex M. Schapelle, VAIOLabs.io


---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is Helm ?
- Who needs Helm ?
- How Helm works ?
- How to manage Helm in various scenarios ?


### Who Is This course for ?

- Junior/senior devops who have no knowledge of k8s deployment
- for junior/senior developers who are still developing on different stacks to containers
- Experienced ops who need refresher k8s deployment with Helm


---

# Course Topics

- Intro
- Helm internals
- Helm basics:
    - Setup Helm
    - Configuration of repository
    - Helm charts
    - Deployments
- Helm advance features


---
# About Me
<img src="../99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.
---

# About Me (cont.)
- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

---
# About Me (cont.)

You can find me on the internet in bunch of places:

- Linkedin: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)
- Gitlab: [Silent-Mobius](https://gitlab.com/silent-mobius)
- Github: [Zero-Pytagoras](https://github.com/zero-pytagoras)
- ASchapelle: [My Site](https://aschapelle.com)
- VaioLabs-IO: [My company site](https://vaiolabs.io)


---

# About You

Share some things about yourself:

- Name and Surname
- Job description
- What type of education do you poses ? formal/informal/self-taught/university/cert-course
- Do you know any of those technologies below ? What level ?
    - Docker / Docker-Compose / K8s
    - Jenkins
    - Git / GitLab / Github / Gitea / Bitbucket
    - Bash Script
    - Python3 / Pytest / Pylint / Flask
    - Go    / Go get    /   Golangci-lint / Gin|Echo|Fiber|Beego|Iris
- Do you have any hobbies ?
- Do you pledge your alliance to [Emperor of Man kind](https://warhammer40k.fandom.com/wiki/Emperor_of_Mankind) ?

---
# History

Kubernetes is a very helpful tool for cloud-native developers. But it doesn't cover all the bases on its own – there are a lot of things that Kubernetes can not solve or that are outside its scope.

This is one of the reasons why open source projects are so great. They help amazing tools become even more amazing when we combine them with other awesome open-source tools. And often these tools were developed for the sole purpose of filling the gaps. One of these tools is **Helm**.

---

# History


- 2003-2004: Birth of the Borg System
    - Google introduced the Borg System around 2003-2004. It started off as a small-scale project, with about 3-4 people initially in collaboration with a new version of Google’s new search engine. Borg was a large-scale internal cluster management system, which ran hundreds of thousands of jobs, from many thousands of different applications, across many clusters, each with up to tens of thousands of machines.
- 2013: From Borg to Omega
    - Following Borg, Google introduced the Omega cluster management system, a flexible, scalable scheduler for large compute clusters. (whitepaper and announcement)
- 2014: Google Introduces Kubernetes
    - mid-2014: Google introduced Kubernetes as an open source version of Borg
    - June 7: Initial release – first github commit for Kubernetes
    - July 10: Microsoft, RedHat, IBM, Docker joins the Kubernetes community.
- 2015: The year of Kube v1.0 & CNCF
    - July 21: Kubernetes v1.0 gets released. Along with the release, Google partnered with the Linux Foundation to form the Cloud Native Computing Foundation (CNCF). The CNFC aims to build sustainable ecosystems and to foster a community around a constellation of high-quality projects that orchestrate containers as part of a microservices architecture.
    - November 3: The Kubernetes ecosystem continues to grow! Companies who joined: **Deis**, OpenShift, Huawei, and Gondor.
    - November 9-11: KubeCon 2015 is the first inaugural community Kubernetes conference in San Fransisco. Its goal was to deliver expert technical talks designed to spark creativity and promote Kubernetes education. You can watch the presentations here on day 1 and day 2.
- 2016: The Year Kubernetes Goes Mainstream!
    - February 23: First release of Helm, the package manager of Kubernetes.
    - September 26: Kubernetes 1.4 introduces a new tool, kubeadm, that helps improve Kubernetes’ installability. This release provides easier setup, stateful application support with integrated Helm, and new cross-cluster federation features.


<!-- https://blog.risingstack.com/the-history-of-kubernetes/ -->

----

# History

Helm is widely known as "the package manager for Kubernetes". Although it presents itself like this, its scope goes way beyond that of a simple package manager. However, let's start at the beginning.

Helm is an open-source project which was originally created by DeisLabs and donated to CNCF, which now maintains it. The original goal of Helm was to provide users with a better way to manage all the Kubernetes YAML files we create on Kubernetes projects.

The path Helm took to solve this issue was to create Helm Charts. Each chart is a bundle with one or more Kubernetes manifests - a chart can have child charts and dependent charts as well.

This means that Helm installs the whole dependency tree of a project if you run the install command for the top-level chart. You just a single command to install your entire application, instead of listing the files to install via kubectl.

Charts allow you to version your manifest files too, just like we do with Node.js or any other package. This lets you install specific chart versions, which means keeping specific configurations for your infrastructure in the form of code.

Helm also keeps a release history of all deployed charts, so you can go back to a previous release if something went wrong.

Helm supports Kubernetes natively, which means you don't have to write any complex syntax files or anything to start using Helm. Just drop your template files into a new chart and you're good to go.

But why should we use it? Managing application manifests can be easily done with a few combinations of commands.

---

# Why Should You Use Helm?

Helm really shines where Kubernetes does not. For instance, *templating*: The scope of the Kubernetes project is to deal with your containers for you, not your template files.

This makes it overly difficult to create truly generic files to be used across a large team or a large organization with many different parameters that need to be set for each file.

And also, how do you version sensitive information using Git when template files are plain text?

The answer: Go templates. Helm allows you to add variables and use functions inside your template files. This makes it perfect for scalable applications that'll eventually need to have their parameters changed. Well see as we go in the course

