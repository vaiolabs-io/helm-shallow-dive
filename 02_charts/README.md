
---

# What is a `Helm` chart?

Helm charts are Helm packages consisting of YAML files and `Go` templates which convert into Kubernetes manifest files. Charts are reusable by anyone for any environment, which reduces complexity and duplicates. Folders have the following structure:
<img src="../99_misc/.img/helm_chart_struct.png" alt="helm_chart_struct" style="float:right;width:400px;">

---

# Explore the `Helm` hub

Just like docker and other system tools, you can search for public Helm charts in the [Hub](https://hub.Helm.sh)
- [hub.helm.sh](https://hub.helm.sh) is the address of the hub
    - We can explore all open source application chart here:
        - Nginx
        - MongoDB
        - Redis
        - Kafka

These projects maintain their own `Helm` charts that you can use, and install in your own Kubernetes cluster.
Each project in hub container notes, just like README files explaining what parameters each helm chart requires and how it can be configured `--set` parameter option.

---

# Install a `Helm` chart in your k8s cluster

Each project that is worked on the kubernetes requires long list of containers wrapped in pods, replica-sets and eventually into deployments. All of these usually require pre made software that gets the cluster up and running. Aside from `kubectl`, which enables us to manage  kubernetes cluster and deploy deployments, `Helm` is the quickest way to install third party applications with all its additional required parts.

Lets see how we can **add** new `Helm` repository

```sh
helm repo add bitnami https://chatrs.bitnami.com/bitnami
```
> `[!]` Note: "bitnami" is the name that we choose to give to repository. it can be anything, but is suggested to keep as informative as possible
> `[!]` Note: last link in the command is the address where the charts can be found

---

# Install a `Helm` chart in your k8s cluster (cont.)

After adding repository, it usually suggested to update the repository on the local machine with `update` command and also we could list the available repositories.

```sh
helm repo update
helm repo list
```
Now that we have access to `bitnami` repo and the repository content is cached, we can try and  install several package for out practice. Let's start by installing `kube-state-metrics` which is a tool that collects data from the Kube APIService about *objects* like **Nodes**, **Deployments**, **Pods** and **ConfigMaps**.
Like with any other kubernetes application it is good practice to create dedicated namespace for each deployment, so lets create `metrics` namespace for our helm chart :

```sh
kubectl create ns metrics
```
Now lets search for the required chart name kube-state-metrics
```sh
helm search hub bitnami kube-state-metrics
```
The `search` command uses `hub` keyword to search chart on bitnami hub and prints to stdout the result.
 
---
# Install a `Helm` chart in your k8s cluster (cont.)

We should choose first chart from the result and install it on our kubernetes cluster

```sh
helm install kube-state-metrics bitnami/kube-state-metrics -n metrics
```
Once command runs, it will pull chart called `kube-state-metrics` from the bitnami repository and install it under name space metrics
> `[!]` Notes:
>    `kube-state-metrics` is name that we choose to give our chart locally
>    `bitnami/kube-state-metrics` is a repository and the name of helm chart we'd like to install out application
>    `-n` is a option noting to which name space we should install the chart

To list the installed charts we use `ls` command with `Helm`.

```sh
helm ls -n metrics
```

We can see the name of the chart as well as additional information about the deployment

---

# Practice

- add repository named `stable` from `Helm`: https://charts.helm.sh/stable
- update the installed repository cache
- search on stable repo wordpress
- search on bitnami repo nginx
- install nginx helm chart from bitnami
- on which namespace was the nginx deployment provided ?

---

# Practice (cont.)

```sh
helm repo add stable  https://charts.helm.sh/stable
helm repo update
helm repo list
helm search hub stable wordpress
helm search hub bitnami nginx
helm install nginx bitnami/nginx
```

---

# Inspect a chart in k8s cluster
It is a good practice to verify the the charts installed under specifying namespace. Let's start by looking at all the **objects** that `kube-state-metrics` created in the `metrics` namespace. 

```sh
kubectl get all -n metrics
```
> `[!]` Note:  we see that the `Helm` chart created one pod, a service, a deployment, and the replica set that determines the number of pods we're running

Let's explore a little more by looking at the logs for the pod. Going to copy the pod name and then run kube control logs, the name of the pod, namespace metrics.
```sh
kubectl logs kube-state-metrics-YOUR-HASH -n metrics
```

Now let's take a look at the data that `kube-state-metrics` is collecting. 

```sh
kubectl port-forward srv/kube-state-metrics 8080:8080 -n metrics # --> access via browser
```

---
# Practice

---

# Try `Helm` show commands

Whether you use someone else helm chart or create your own, it is some what rare that you will require some type of customization or at least minor change. to view  possible options that the chart provide itself, we could use `helm show` command.

```sh
helm show help
helm show chart 
```
`helm show all` gives us all the information about the chart in one view, while `helm show chart` displays high level information about the chart like its version, a brief description and who maintains the chart

Due to plenty information that can be found in `helm show chart` command, it is suggested to  pipe the output to yaml file, usually called `values.yml`, thus also suggested to check with text editor that can color  the values in the file.
```sh
help show chart bitnami/kube-state-metrics > values.yml
vim values.yml
```
> `[!]` Note: I am using vim to see values, you can use what ever you wish
---

# Practice

---
# Update `Helm` chart

One part where `helm` shines is deploying to new version of created application. `Helm` has commands that let you quickly install a different version of a chart. Right now we're running kube-state-metrics chart version 0.4.3 which we can verify with :
```sh
helm ls -n metrics
```
In case we would like to downgrade or upgrade version of our application we can use `helm` chart, with `--version` flag with required version. As an example:
```sh
helm upgrade kube-state-metrics bitnami/kube-state-metrics --version 0.4.0 -n metrics
```
To verify that the pods were updated, lets verify that all has been updated.
```sh
kubectl get all -n metrics
``` 

---

#  Practice

- Create new name space called monitor
- Install the Bitnami-hosted metrics server helm chart
- Ensure the release name is monitor-metrics-server
- Enable the metrics API with the flag `--set apiServer.create=true`

---

# Practice

```sh
kubectl create ns monitor
helm install monitor-metrics-server bitnami/metric-server --set apiService.create=true -n monitor
kubectl get all -n monitor
```

--- 
# Summary Practice

